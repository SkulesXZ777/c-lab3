﻿using System;
using System.Windows.Forms;

namespace CSharpLab3
{
    public partial class Form3 : Form
    {
        private Form1 perent = null;
        private int row;

        public void setPerent(Form1 perent)
        {
            this.perent = perent;
        }

        public void setRow(int row)
        {
            this.row = row;
        }

        public void setTextBox1Text(string text)
        {
            textBox1.Text = text;
        }

        public void setTextBox2Text(string text)
        {
            textBox2.Text = text;
        }

        public void setTextBox3Text(string text)
        {
            textBox3.Text = text;
        }

        public void setButton1Visible(bool value)
        {
            this.button1.Visible = value;
        }

        public void setButton2Visible(bool value)
        {
            this.button2.Visible = value;
        }

        public Form3()
        {
            InitializeComponent();
        }

        //Добавить группу
        private void button1_Click_1(object sender, EventArgs e)
        {
            perent.AddGroup(textBox1.Text, textBox2.Text, textBox3.Text);
            perent.FillDataGridView1ByGroups();
            this.Visible = false;
        }

        //Заменить группу
        private void button2_Click_1(object sender, EventArgs e)
        {
            perent.UpdateGroup(row, textBox1.Text, textBox2.Text, textBox3.Text);
            perent.FillDataGridView1ByGroups();
            this.Visible = false;
        }
    }
}