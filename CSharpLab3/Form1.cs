﻿using System;
using System.Data;
using System.Windows.Forms;
using Npgsql;

namespace CSharpLab3
{
    public partial class Form1 : Form
    {
        //Ссылка на подключение
        private NpgsqlConnection connection = null;
        //Ссылка на DataSet
        private DataSet dataSet = null;
        //Ссылки на DataAdapter для группы и студента
        private NpgsqlDataAdapter groupDataAdapter = null;
        private NpgsqlDataAdapter studentDataAdapter = null;
        //Ссылки на вспомогательные формы
        private Form2 form2 = null;
        private Form3 form3 = null;
        private Form4 form4 = null;

        public void setConnection(NpgsqlConnection connection)
        {
            this.connection = connection;
        }

        //Создание DataSet
        private DataSet getDataSet()
        {
            if (dataSet == null)
            {
                dataSet = new DataSet();
                dataSet.Tables.Add("Groups");
                dataSet.Tables.Add("Students");
            }
            return dataSet;
        }

        //Получить форму установления соединения
        public Form2 getForm2()
        {
            if (form2 == null)
            {
                form2 = new Form2();
                form2.setPerent(this);
            }
            return form2;
        }

        //Получить форму добавления и замены группы
        public Form3 getForm3()
        {
            if (form3 == null)
            {
                form3 = new Form3();
                form3.setPerent(this);
            }
            return form3;
        }

        //Получить форму добавления и замены студента
        public Form4 getForm4()
        {
            if (form4 == null)
            {
                form4 = new Form4();
                form4.setPerent(this);
            }
            return form4;
        }

        //Установить соединение с базой
        public NpgsqlConnection Connect(string host, int port, string database,
            string user, string parol)
        {
            NpgsqlConnectionStringBuilder stringBuilder =
                new NpgsqlConnectionStringBuilder();
            stringBuilder.Host = host;
            stringBuilder.Port = port;
            stringBuilder.Username = user;
            stringBuilder.Password = parol;
            stringBuilder.Database = database;
            stringBuilder.Timeout = 30;
            NpgsqlConnection connection =
                new NpgsqlConnection(stringBuilder.ConnectionString);
            connection.Open();
            return connection;
        }

        //Заполнить DataGridView1 студентами
        public void FillDataGridView1ByGroups()
        {
            getDataSet().Tables["Groups"].Clear();
            groupDataAdapter = new NpgsqlDataAdapter(
                "SELECT * FROM Groups", connection);
            new NpgsqlCommandBuilder(groupDataAdapter);
            groupDataAdapter.Fill(getDataSet(), "Groups");
            dataGridView1.DataSource = getDataSet().Tables["Groups"];
        }

        //Заполнить DataGridView2 студентами заданной группы
        public void FillDataGridView2ByStudents(string groupName)
        {
            getDataSet().Tables["Students"].Clear();
            studentDataAdapter = new NpgsqlDataAdapter(
                "SELECT Students.id, firstname, lastname, sex, age, groupid " +
                "FROM Groups, Students " +
                "WHERE Groups.id = Students.groupid AND groupname = '" +
                groupName + "'", connection);
            new NpgsqlCommandBuilder(studentDataAdapter);
            studentDataAdapter.Fill(dataSet, "Students");
            dataGridView2.DataSource = getDataSet().Tables["Students"];
        }

        //GROUPS ******************************************************************


        //Добавить группу
        public void AddGroup(string groupName,
            string curatorName, string headmanName)
        {
            getDataSet().Tables["Groups"].Rows.Add(0, groupName,
                curatorName, headmanName);
            groupDataAdapter.Update(getDataSet(), "Groups");
        }

        //Заменить группу
        public void UpdateGroup(int row, string groupName,
            string curatorName, string headmanName)
        {
            getDataSet().Tables["Groups"].Rows[row]["groupname"] = groupName;
            getDataSet().Tables["Groups"].Rows[row]["curatorname"] = curatorName;
            getDataSet().Tables["Groups"].Rows[row]["headmanname"] = headmanName;
            groupDataAdapter.Update(getDataSet(), "Groups");
        }

        //*******************STUDENTS******************************************


        //Добавить студента
        public void AddStudent(string firstName,
            string secondName, string sex, int age, long groupId)
        {
            getDataSet().Tables["Students"]
                .Rows.Add(0, firstName, secondName, sex, age, groupId);
            studentDataAdapter.Update(getDataSet(), "Students");
        }

        //Заменить студента
        public void UpdateStudent(int row, string firstName,
            string secondName, string sex, int age)
        {
            getDataSet().Tables["Students"].Rows[row]["firstname"] = firstName;
            getDataSet().Tables["Students"].Rows[row]["lastname"] = secondName;
            getDataSet().Tables["Students"].Rows[row]["sex"] = sex;
            getDataSet().Tables["Students"].Rows[row]["age"] = age;
            studentDataAdapter.Update(getDataSet(), "Students");
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void подключитьсяКБДToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            getForm2().Visible = true;
        }

        private void отключитьсяОтБДToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            connection.Close();
        }

        private void добавитьToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            getForm3().Visible = true;
            getForm3().setButton1Visible(true);
            getForm3().setButton2Visible(false);
            getForm3().setTextBox1Text("");
            getForm3().setTextBox2Text("");
            getForm3().setTextBox3Text("");
        }

        private void удалитьToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            int selectedRow = dataGridView1.SelectedCells[0].RowIndex;
            DialogResult dr = MessageBox.Show("Удалить группу?", "",
                MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                getDataSet().Tables["Groups"].Rows[selectedRow].Delete();
                groupDataAdapter.Update(getDataSet(), "Groups");
                getDataSet().Clear();
                FillDataGridView1ByGroups();
            }
        }

        private void заменитьToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            int selectedRow = dataGridView1.SelectedCells[0].RowIndex;
            string groupName = (string) getDataSet().Tables["Groups"]
                .Rows[selectedRow].ItemArray[1];
            string curatorName = (string) getDataSet().Tables["Groups"]
                .Rows[selectedRow].ItemArray[2];
            string headmanName = (string) getDataSet().Tables["Groups"]
                .Rows[selectedRow].ItemArray[3];
            getForm3().Visible = true;
            getForm3().setButton1Visible(false);
            getForm3().setButton2Visible(true);
            getForm3().setTextBox1Text(groupName);
            getForm3().setTextBox2Text(curatorName);
            getForm3().setTextBox3Text(headmanName);
            getForm3().setRow(selectedRow);
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int selectedRow = dataGridView1.SelectedCells[0].RowIndex;
            string key = (string) dataGridView1.Rows[selectedRow].Cells[1].Value;
            FillDataGridView2ByStudents(key);
        }

        //Добавить
        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            getForm4().Visible = true;
            getForm4().setButton1Visible(true);
            getForm4().setButton2Visible(false);
            getForm4().setTextBox1Text("");
            getForm4().setTextBox2Text("");
            getForm4().setTextBox3Text("");
            getForm4().setTextBox4Text("");
            int selectedRow = dataGridView1.SelectedCells[0].RowIndex;
            long groupId = (long) getDataSet().Tables["Groups"]
                .Rows[selectedRow].ItemArray[0];
            string groupName = (string) getDataSet().Tables["Groups"]
                .Rows[selectedRow].ItemArray[1];
            getForm4().setGroupName(groupName);
            getForm4().setGroupId(groupId);
        }

        //Удалить
        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            int selectedRow = dataGridView2.SelectedCells[0].RowIndex;
            DialogResult dr = MessageBox.Show("Удалить студента?", "",
                MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                getDataSet().Tables["Students"].Rows[selectedRow].Delete();
                studentDataAdapter.Update(getDataSet(), "Students");
                string key = (string) dataGridView1
                    .Rows[selectedRow].Cells[1].Value;
                FillDataGridView2ByStudents(key);
            }
        }

        //Заменить
        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            int selectedRow = dataGridView2.SelectedCells[0].RowIndex;
            string firstName = (string) getDataSet().Tables["Students"]
                .Rows[selectedRow].ItemArray[1];
            string secondName = (string) getDataSet().Tables["Students"]
                .Rows[selectedRow].ItemArray[2];
            string sex = (string) getDataSet().Tables["Students"]
                .Rows[selectedRow].ItemArray[3];
            int age = (int) getDataSet().Tables["Students"]
                .Rows[selectedRow].ItemArray[4];
            string sAge = System.Convert.ToString(age);
            getForm4().Visible = true;
            getForm4().setButton1Visible(false);
            getForm4().setButton2Visible(true);
            getForm4().setTextBox1Text(firstName);
            getForm4().setTextBox2Text(secondName);
            getForm4().setTextBox3Text(sex);
            getForm4().setTextBox4Text(sAge);
            getForm4().setRow(selectedRow);
            int selectedRow1 = dataGridView1.SelectedCells[0].RowIndex;
            string groupName = (string) getDataSet().Tables["Groups"]
                .Rows[selectedRow1].ItemArray[1];
            getForm4().setGroupName(groupName);
        }
    }
}