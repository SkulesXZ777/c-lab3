﻿using System;
using System.Windows.Forms;
using Npgsql;

namespace CSharpLab3
{
    public partial class Form2 : Form
    {
        private Form1 perent;

        public void setPerent(Form1 perent)
        {
            this.perent = perent;
        }

        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            string server = textBox1.Text;
            int port = Int32.Parse(textBox2.Text);
            string database = textBox3.Text;
            string user = textBox4.Text;
            string passwd = textBox5.Text;
            NpgsqlConnection connection =
                perent.Connect(server, port, database, user, passwd);
            perent.setConnection(connection);
            perent.FillDataGridView1ByGroups();
            this.Visible = false;
        }
    }
}