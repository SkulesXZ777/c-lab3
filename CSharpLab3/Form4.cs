﻿using System;
using System.Windows.Forms;

namespace CSharpLab3
{
    public partial class Form4 : Form
    {
        private Form1 perent = null;
        private string groupName;
        private long groupId;
        private int row;

        public void setPerent(Form1 perent)
        {
            this.perent = perent;
        }

        public void setGroupName(string groupName)
        {
            this.groupName = groupName;
        }

        public void setGroupId(long groupId)
        {
            this.groupId = groupId;
        }

        public void setRow(int row)
        {
            this.row = row;
        }

        public void setTextBox1Text(string text)
        {
            textBox1.Text = text;
        }

        public void setTextBox2Text(string text)
        {
            textBox2.Text = text;
        }

        public void setTextBox3Text(string text)
        {
            textBox3.Text = text;
        }

        public void setTextBox4Text(string text)
        {
            textBox4.Text = text;
        }

        public void setButton1Visible(bool value)
        {
            this.button1.Visible = value;
        }

        public void setButton2Visible(bool value)
        {
            this.button2.Visible = value;
        }

        public Form4()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            perent.AddStudent(textBox1.Text, textBox2.Text,
                textBox3.Text, Int16.Parse(textBox4.Text), groupId);
            perent.FillDataGridView2ByStudents(groupName);
            this.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            perent.UpdateStudent(row, textBox1.Text, textBox2.Text,
                textBox3.Text, System.Convert.ToInt32(textBox4.Text));
            perent.FillDataGridView2ByStudents(groupName);
            this.Visible = false;
        }
    }
}